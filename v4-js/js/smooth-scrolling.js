const links = document.querySelectorAll('a[href^="#"]');
 
for (const link of links) {
  link.addEventListener("click", clickHandler);
}
 
function clickHandler(e) {
  e.preventDefault();
  const href = this.getAttribute("href");
 
  /**
   * Window.scroll() 
   */
  const offsetTop = document.querySelector(href).offsetTop;
  scroll({
    top: offsetTop,
    behavior: "smooth"
  });

  /**
   * Element.scrollIntoView()
   */
//   document.querySelector(href).scrollIntoView({
//     behavior: "smooth"
//   });

}
