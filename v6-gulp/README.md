# Gulp

Postup vytvoření:
1. devDependencies:
   ```
   npm install --save-dave gulp-cli
   npm init -y
   npm install --save-dev gulp@4.0.0 gulp-babel @babel/register @babel/core @babel/preset-env
   npm install --save-dev gulp-imagemin@7.1.0
   npm install --save-dev gulp-concat gulp-uglify gulp-rename gulp-clean-css del
   npm install --save-dev sass gulp-sass
   npm install --save-dev gulp-htmlmin gulp-ejs
   npm install --save-dev browserify
   npm install --save-dev babelify
   npm install --save-dev gulp-notify
   npm install --save-dev vinyl-source-stream
   npm install --save-dev @babel/plugin-transform-runtime
   npm install --save-dev @babel/plugin-proposal-object-rest-spread
   ```
1. dependencies:
   ```
   npm install core-js
   npm install css-vars-ponyfill
   npm install smoothscroll-polyfill
   ```
1. .babelrc
1. gulpfile.babel.js
1. ```
   gulp
   ```