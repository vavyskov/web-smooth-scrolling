import gulp from 'gulp';
import babel from 'gulp-babel';
import concat from 'gulp-concat';
import uglify from 'gulp-uglify';
import rename from 'gulp-rename';
import cleanCSS from 'gulp-clean-css';
import del from 'del';
import imagemin from 'gulp-imagemin';
import htmlmin from 'gulp-htmlmin';
import ejs from 'gulp-ejs';

import dartSass from 'sass';
import gulpSass from 'gulp-sass';
const sass = gulpSass(dartSass);

import browserify from 'browserify';
import babelify from 'babelify';
import notify from 'gulp-notify';
import source from 'vinyl-source-stream';
 
const paths = {
  html: {
    src: 'src/**/*.html',
    dest: 'public/'
  },  
  styles: {
    src: 'src/css/**/*.{css,scss}',
    dest: 'public/css/'
  },
  scripts: {
    src: [
      'src/js/**/*.js',
      '!src/js/**/_*'
    ],
    dest: 'public/js/'
  },
  images: {
    src: 'src/assets/images/**/*.{jpg,jpeg,png,gif,svg}',
    dest: 'public/assets/images/'
  }
};
 
/*
 * For small tasks you can export arrow functions
 */
export const clean = () => del([ 'public' ]);

// HTML (EJS template, Minify)
export function html() {
    return gulp.src(paths.html.src)
        .pipe(ejs({}, {}, { ext: '.html' }))
		.pipe(htmlmin({ 
			collapseWhitespace: true,
			removeComments: true,
            minifyJS: true,
            minifyCSS: true
		}))
        .pipe(gulp.dest(paths.html.dest))
        // .pipe(browserSync.stream());
}

/*
 * You can also declare named functions and export them as tasks
 */
export function styles() {
  return gulp.src(paths.styles.src)
	.pipe(sass().on('error', sass.logError))
    .pipe(cleanCSS())
    .pipe(rename({
    //   basename: 'main',
    //   suffix: '.min'
    }))
    .pipe(gulp.dest(paths.styles.dest));
}
 




/*export function scripts() {
  return gulp.src(paths.scripts.src, { sourcemaps: true })
    .pipe(babel({
      "presets": [
        [
          "@babel/env",
          {
            // "modules": "commonjs",
            "useBuiltIns": "usage", // automatically import polyfil code in each file that needs it
            "corejs": 3
          },
        ],
      ],
    }))
    .pipe(uglify())
    // .pipe(concat('main.min.js'))
    .pipe(gulp.dest(paths.scripts.dest));
}*/

export function scripts() {
  return browserify('src/js/index.js')
  .transform(babelify, {
    presets: [
      '@babel/preset-env'
    ], 
    plugins: [
      '@babel/plugin-transform-runtime', 
      '@babel/plugin-proposal-object-rest-spread'
    ], 
    sourceMapsAbsolute: true 
  })
  .bundle()
  .on('error', notify.onError(function (error) {
      return "JS: " + error.message;
  }))
  .pipe(source('index.js'))
  .pipe(gulp.dest(paths.scripts.dest));
}




 
 /*
  * You could even use `export as` to rename exported tasks
  */
function watchFiles() {
  gulp.watch(paths.scripts.src, scripts);
  gulp.watch(paths.styles.src, styles);
}
export { watchFiles as watch };





function images() {
	return gulp.src(paths.images.src, {since: gulp.lastRun(images)})
	  .pipe(imagemin({optimizationLevel: 5}))
	  .pipe(gulp.dest(paths.images.dest));
}

function watch() {
	gulp.watch(paths.images.src, images);
}





const build = gulp.series(clean, gulp.parallel(html, styles, scripts, images));
/*
 * Export a default task
 */
export default build;
