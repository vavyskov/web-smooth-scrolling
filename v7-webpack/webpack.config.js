const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const FixStyleOnlyEntriesPlugin = require('webpack-fix-style-only-entries');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const webpack = require('webpack');

const devMode = process.env.NODE_ENV !== 'production';

module.exports = {
    //mode: 'development',
    //mode: process.env.NODE_ENV,
    devtool: 'source-map',
    stats: 'errors-only',
    entry: {



        //global: path.resolve(__dirname, 'src/css', 'global.scss'),
//        global: path.resolve(__dirname, 'src/js', 'global.js'),
        //index: path.resolve(__dirname, 'src/css', 'index.scss'),
        index: path.resolve(__dirname, 'src/js', 'index.js'),
        //'o-nas': path.resolve(__dirname, 'src/css', 'o-nas.scss'),
//        'o-nas': path.resolve(__dirname, 'src/js', 'o-nas.js'),
        //kontakt: path.resolve(__dirname, 'src/css', 'kontakt.scss'),
//        kontakt: path.resolve(__dirname, 'src/js', 'kontakt.js'),

        //polyfill: path.resolve(__dirname, 'src/css', 'polyfill.scss'),
        //polyfill: path.resolve(__dirname, 'src/js', 'polyfill.js'),



    },
    output: {
        path: path.resolve(__dirname, 'public'),
        filename: 'js/[name].[contenthash].js',
        chunkFilename: 'js/[id].[contenthash].js',
    },
    plugins: [
        new webpack.ProgressPlugin(),
        new CleanWebpackPlugin(),



        new HtmlWebpackPlugin({
            template: path.resolve(__dirname, 'src', 'index.html'),
            //template: path.resolve(__dirname, 'src', 'index.ejs'),
            //template: '!!ejs-loader!./src/index.ejs',
            //template: 'ejs-loader!./src/index.ejs',
            //template: 'ejs!./src/index.ejs',
            //template: '!!ejs!./src/index.ejs',
            //template: './src/index.ejs',
            //inject: 'body',
            chunks: ['global', 'index'],
            filename: 'index.html',
        }),
//        new HtmlWebpackPlugin({
//            template: path.resolve(__dirname, 'src', 'o-nas.html'),
//            //inject: 'body',
//            chunks: ['global', 'o-nas'],
//            filename: 'o-nas.html',
//        }),
//        new HtmlWebpackPlugin({
//            template: path.resolve(__dirname, 'src', 'kontakt.html'),
//            //inject: 'body',
//            chunks: ['global', 'kontakt'],
//            filename: 'kontakt.html',
//        }),

        // new HtmlWebpackPlugin({
        //     template: path.resolve(__dirname, 'src', 'polyfill.html'),
        //     //inject: 'body',
        //     chunks: ['global', 'polyfill'],
        //     filename: 'polyfill.html',
        // }),



        new FixStyleOnlyEntriesPlugin(), // Disable global JavaScript file (from entry global.scss)
        new MiniCssExtractPlugin({
            filename: 'css/[name].[contenthash].css',
            chunkFilename: 'css/[id].[contenthash].css',
        }),
    ],
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: ['babel-loader'],
            },
            {
                test: /\.(sa|sc|c)ss$/,
                exclude: /node_modules/,
                use: [
                    // Inject CSS to page (development mode) or extract CSS as a file (production mode)
                    devMode
                        ? 'style-loader'
                        : MiniCssExtractPlugin.loader,
                        // : {
                        //     loader: MiniCssExtractPlugin.loader,
                        //         options: {
                        //         publicPath: (resourcePath, context) => {
                        //             // publicPath is the relative path of the resource to the context
                        //             // e.g. for ./css/admin/main.css the publicPath will be ../../
                        //             // while for ./css/main.css the publicPath will be ../
                        //             return path.relative(path.dirname(resourcePath), context) + '/';
                        //         },
                        //     },
                        // },
                    {
                        // Translates CSS into CommonJS modules
                        loader: 'css-loader',
                        options: {
                            importLoaders: 1,
                        },
                    },
                    {
                        // Run postcss actions
                        loader: 'postcss-loader',
                        options: {
                            postcssOptions: {
                                plugins: [
                                    [
                                        'autoprefixer',
                                        {
                                            // Options
                                        },
                                    ],
                                ],
                            },
                        },
                    },
                    // Compiles Sass to CSS
                    //'sass-loader',
                    {
                        // Compiles Sass to CSS
                        loader: 'sass-loader',
                    }
                ]
            },
            /*{
                test: /\.ejs$/,
                exclude: /node_modules/,
                //loader: 'ejs-loader?variable=data'
                loader: 'ejs-loader',
                options: {
                    //esModule: false,
                    variable: 'data',
                    //interpolate : '\\{\\{(.+?)\\}\\}',
                    //evaluate : '\\[\\[(.+?)\\]\\]'
                    interpolate: /<\$=([\s\S]+?)\$>/g,
                    evaluate: /<\$([\s\S]+?)\$>/g,
                }
            }*/
        ]
    },
    optimization: {
        splitChunks: {
            chunks: 'all',
            name: false,
            ////maxSize: 244000,

            // cacheGroups: {
            //     commons: {
            //         test: /[\\/]node_modules[\\/]/,
            //         name: 'vendor',
            //         chunks: 'all',
            //     }
            // }
        }
    },
};
