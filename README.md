# WEB Smooth Scrolling

Plynulé posouvání

Možnosti:
- CSS
  - https://developer.mozilla.org/en-US/docs/Web/CSS/scroll-behavior
  - https://caniuse.com/css-scroll-behavior
- jQuery
  - https://jquery.com/browser-support/
  - https://caniuse.com/sr_jquery
- JS
  - Window.scroll()
    - https://developer.mozilla.org/en-US/docs/Web/API/Window/scroll
    - https://caniuse.com/mdn-api_window_scroll
  - Element.scrollIntoView()
    - https://developer.mozilla.org/en-US/docs/Web/API/Element/scrollIntoView
    - https://caniuse.com/scrollintoview
- Polyfill
  - https://github.com/iamdustan/smoothscroll
  - https://www.npmjs.com/package/css-vars-ponyfill
    - https://caniuse.com/css-variables
- Gulp + Polyfill
- Webpack + Polyfill
- ESBuild + Polyfill (transforming "for-of loops" is not supported yet)
  - ```
    npm init -y
    npm install --save-dev esbuild
    ```

ToDo:
- React + Polyfill